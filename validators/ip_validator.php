<?php 



function ip_validator($value): bool 
{
    $result = false;

    if (filter_var($value, FILTER_VALIDATE_IP)) {
        $result = true;
    }

    return $result;
}
