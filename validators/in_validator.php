<?php 

function in_validator($value, $list):bool 
{

    $result = false;

    if (in_array($value, $list)) {
        $result = true;
    }

    return $result;
}
