<?php 


function between_validator($value, $start, $end): bool 
{
    $result = false;

    if($value >= $start && $value <= $end){
        $result = true;
    }

    return $result;
}
