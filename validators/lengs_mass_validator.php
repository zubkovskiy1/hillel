<?php 

function lengs_mass_validator($value, $min, $max): bool 
{
    $result = false;

    if (mb_strlen($value) >= $min && mb_strlen($value) <= $max) {
        $result = true;
    }

    return $result;
}
