<?php 



function min_lengs_validator($value, $min): bool 
{
    $result = false;

    if (strlen($value) >= $min) {
        $result = true;
    }

    return $result;
}
