<?php 


function email_validator($value):bool
{
    $result = false;

    if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
        $result = true;
    }
    return $result;
}
