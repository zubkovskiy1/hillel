<?php 




function url_validator($value):bool 
{
    $result = false;

    if(filter_var($value, FILTER_VALIDATE_URL)){
        $result = true;
    }

    return $result;

}
