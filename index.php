<?php

require_once 'validators/not_empty_validator.php';
require_once 'validators/email_validator.php';
require_once 'validators/is_equal_validator.php';
require_once 'validators/min_lengs_validator.php';
require_once 'validators/lengs_mass_validator.php';



$errors = [
   'name' => [],
   'email' => [],
   'subject' => [],
   'message' => [],
];

if($_SERVER['REQUEST_METHOD'] == 'POST'){

   if(!not_empty_validator($_POST['name'])){
      $errors['name'][] = 'Почта не должна быть пустой';
  }

   if(!not_empty_validator($_POST['email'])){
       $errors['email'][] = 'Почта не должна быть пустой';
   }else{
       if(!email_validator($_POST['email'])){
           $errors['email'][] = 'Неверный формат почты';
       }
   }
   

   if(!not_empty_validator($_POST['subject'])){
       $errors['subject'][] = 'Пароль не должн быть пустой';
   }else{
      if(!min_lengs_validator($_POST['subject'], 6)){
         $errors['subject'][] = "тема письма должна быть не менее 10 символов";
      }
   }

   
   if(!not_empty_validator($_POST['message'])){
       $errors['message'][] = 'Подтверждение пароля не должно быть пустым';
   }else{
      if(!lengs_mass_validator($_POST['message'], 50, 100)){
         $errors['message'][] = "сообщение должно быть не менее 50 и не более 100 символов";
      }
   }

}
var_dump($errors);



session_start();

$token = md5(uniqid());
$_SESSION['csrf_token'] = $token;



$firstName = "Matty";
$lastName = "Jamson";
$mainDescription = "UX/UI Designer";


$aboutaboutTitle = 'About Me';
$aboutDeckription = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis volutpat nec tortor a eleifend. Proin nec ligula vel sem luctus porttitor. Cras at interdum libero. Donec nec mauris velit Vestibulum eu eros tortor. Aliquam cursus nunc mauris, nec congue tortor pretium et. Pellentesque feugiat justo in metus laoreet consectetur. Mauris at tempor ipsum, sit amet etae posuere. Nunc auctor sollicitudin sem ut molestie. Pellentesque ligula sapien, ullamcorper et tempor id, congue ac sapien.';

$personalSkills = [
   [
      'skillName'    => 'Communication',
      'skillProcent' => '85'
   ],
   [
      'skillName'    => 'Team work',
      'skillProcent' => '80'
   ],
   [
      'skillName'    => 'Leadership',
      'skillProcent' => '95'
   ],
   [
      'skillName'    => 'Management',
      'skillProcent' => '85'
   ]
];

$professionalSkills = [
   [
      'skillName'  => 'Create Wireframe',
      'skillScore' => 9
   ],
   [
      'skillName'  => 'Axure',
      'skillScore' => '8'
   ],
   [
      'skillName'  => 'Illustrator',
      'skillScore' => '9'
   ],
   [
      'skillName'  => 'Photoshop',
      'skillScore' => '7'
   ],
   [
      'skillName'  => 'Digital Marketing',
      'skillScore' => '4'
   ]
];

$work = [
   [
      "startDate"    => "01-01-2013",
      "endDate"      => "01-09-2019",
      "company"      => "Flash media inc.",
      "designation"  => "SENIOR UX/UI DESIGNER",
      "description"  => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem."
   ],
   [
      "startDate"   => "01-03-2011",
      "endDate"     => "01-12-2012",
      "company"     => "Codedash Studio",
      "designation" => "UX DESIGNER",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem."
   ],
   [
      "startDate"   => "01-07-2008",
      "endDate"     => "01-04-2011",
      "company"     => "Foursqure Studio",
      "designation" => "VISUAL / UI DESIGNER",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem."
   ], 
];



$workLen = count($work);
 
foreach($work as &$item){
   $start = new DateTime($item['startDate']);
   $end = new DateTime($item['endDate']);
   $period = $end->diff($start);

   $item['startDate'] = $start->format('F y');
   $item['endDate'] = $end->format('F y');
   $item['period'] = $period->format('%y years %m months ');
}
unset($item);

$education = [
   [
      "date"        => "01-01-2007",
      "company"     => "Master Degree Of Design",
      "designation" => "UNIVERSITY OF DESIGN",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem."
   ],
   [
      "date"        => "01-07-2004",
      "company"     => "Bachelor Of Arts",
      "designation" => "UNIVERSITY OF DESIGN",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem."
   ],
   [
      "date"        => "01-01-2001",
      "company"     => "Master Degree Of Design",
      "designation" => "UNIVERSITY OF DESIGN",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem."
   ],
   [
      "date"        => "01-07-2008",
      "company"     => "Bachelor Of Arts",
      "designation" => "UNIVERSITY OF DESIGN",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem."
   ]
];


foreach($education as &$item){
   $start = new DateTime($item['date']);
   $item['date'] = $start->format('F y');
   
}
unset($item);


$references = [
   [
      "testimonial" => "“Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.”",
      "referencImg" => "http://via.placeholder.com/100x100",
      "userDetail"  => "Jonathan doe",
      "designation" => "PROJECT MANAGER",
      "company"     => "MATRIX MEDIA"
   ],
   [
      "testimonial" => "“Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.”",
      "referencImg" => "http://via.placeholder.com/100x100",
      "userDetail"  => "Jonathan doe",
      "designation" => "PROJECT MANAGER",
      "company"     => "MATRIX MEDIA"
   ],
   [
      "testimonial" => "“Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.”",
      "referencImg" => "http://via.placeholder.com/100x100",
      "userDetail"  => "Jonathan doe",
      "designation" => "PROJECT MANAGER",
      "company"     => "MATRIX MEDIA"
   ],
   [
      "testimonial" => "“Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.”",
      "referencImg" => "http://via.placeholder.com/100x100",
      "userDetail"  => "Jonathan doe",
      "designation" => "PROJECT MANAGER",
      "company"     => "MATRIX MEDIA"
   ],
   [
      "testimonial" => "“Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.”",
      "referencImg" => "http://via.placeholder.com/100x100",
      "userDetail"  => "Jonathan doe",
      "designation" => "PROJECT MANAGER",
      "company"     => "MATRIX MEDIA"
   ],
   [
      "testimonial" => "“Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.”",
      "referencImg" => "http://via.placeholder.com/100x100",
      "userDetail"  => "Jonathan doe",
      "designation" => "PROJECT MANAGER",
      "company"     => "MATRIX MEDIA"
   ],
];
   
?><!DOCTYPE html>
<html lang="zxx">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="Responsive Landing Page for Presenting Your Resume">
      <meta name="keywords" content="one page, responsive, html template, responsive landing page">
      <meta name="author" content="ThinkingCoder">
      <title>Flatr - vCard, CV, Resume & Portfolio template</title>
      <!-- FAVICON  -->
      <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
      <link rel="icon" href="favicon.ico" type="image/x-icon">
      <!-- Custom Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900" rel="stylesheet">
      <!-- Bootstrap Core CSS -->
      <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="lib/bootstrap/css/normalize.css" rel="stylesheet">
      <!-- Plugin CSS -->
      <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css">
      <link rel="stylesheet" href="lib/wowjs/animate.min.css">
      <link rel="stylesheet" href="lib/pretty-photo/prettyPhoto.css">
      <!-- Template CSS -->
      <link rel="stylesheet" id="colors" href="css/layout1-color1.css" type="text/css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <!-- =========================================
         Navigation
         ========================================== -->
      <!--start mobile navigation-->
      <div class="nav visible-xs fixed-top">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <button class="btn btn-outline inverse btn-sm navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">Menu
                  <i class="fa fa-bars ico-sm"></i></button>
               </div>
               <div class="collapse navbar-collapse" id="navbarResponsive">
                  <ul class="navbar-nav text-uppercase">
                     <li class="page-scroll nav-item">
                        <a class="nav-link scroll-to" data-toggle="collapse" data-target="#navbarResponsive" href="#introduction">introduction</a>
                     </li>
                     <li class="page-scroll nav-item">
                        <a class="nav-link scroll-to" data-toggle="collapse" data-target="#navbarResponsive" href="#skillset">skillset</a>
                     </li>
                     <li class="page-scroll nav-item">
                        <a class="nav-link scroll-to" data-toggle="collapse" data-target="#navbarResponsive" href="#experience">experience</a>
                     </li>
                     <li class="page-scroll nav-item">
                        <a class="nav-link scroll-to" data-toggle="collapse" data-target="#navbarResponsive" href="#portfolio">portfolio</a>
                     </li>
                     <li class="page-scroll nav-item">
                        <a class="nav-link scroll-to" data-toggle="collapse" data-target="#navbarResponsive" href="#references">references</a>
                     </li>
                     <li class="page-scroll nav-item">
                        <a class="nav-link scroll-to" data-toggle="collapse" data-target="#navbarResponsive" href="#connect">connect</a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--end mobile navigation-->
      <nav class="nav-container hidden-xs">
         <ul class="nav">
            <li class="page-scroll">
               <a href="#introduction">introduction</a>
            </li>
            <li class="page-scroll">
               <a href="#skillset">skills</a>
            </li>
            <li class="page-scroll">
               <a href="#experience">experience</a>
            </li>
            <li class="page-scroll">
               <a href="#portfolio">portfolio</a>
            </li>
            <li class="page-scroll">
               <a href="#references">references</a>
            </li>
            <li class="page-scroll">
               <a href="#connect">connect</a>
            </li>
         </ul>
      </nav>
      <!-- =========================================
         Main Container
         ========================================== -->
      <div class="container main-container">
         <div class="row">
            <!-- =========================================
               HEADER
               ========================================== -->
            <header>
               <div class="col-md-12 extra-offset-md">
                  <button class="btn btn-transparent">
                     <svg>
                        <use xlink:href="img/svg/icons.svg#download"></use>
                     </svg>
                     Download resume
                  </button>
                  <div class="resume-title">
                     <h2><?= $firstName ?></h2>
                     <h2><?= $lastName ?></h2>
                     <div class="resume-designation extra-offset-md">
                        <span class="border"></span>
                        <span><?= $mainDescription ?></span>
                     </div>
                  </div>
               </div>
               <div class="header-overlay"></div>
            </header>
            <!-- =========================================
               ABOUT ME
               ========================================== -->
            
            <section id="introduction">
               <div class="col-md-offset-1 col-md-10">
                  <h2 class="title"><?php echo  $aboutaboutTitle ?></h2>
                  <hr>
                  <p><?php echo $aboutDeckription ?></p>
               </div>
            </section>
            <!-- =========================================
               SKILL
               ========================================== -->
            <section id="skillset">
               <div class="col-md-offset-1 col-md-5 offset-lt">
                  <h2 class="title">Personal Skills</h2>
                  <hr>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis volutpat nec.</p>
                  <div class="skill-wrapper">
                     <!--skill-->
                     <ul class="skill" id="skills">
                     <?php foreach ($personalSkills as $skill): ?>
                        <li>
                           <p><?php echo $skill['skillName']; ?></p>
                           <div class="skill-bar wow slideInLeft" data-width="<?php echo $skill['skillProcent']; ?>">
                              <span class="skill-tip"></span>
                           </div>
                        </li>
                        <?php endforeach; ?>
                     </ul>
                  </div>
               </div>
               <div class="col-md-5 offset-rt">
                  <h2 class="title">Professional Skills</h2>
                  <hr>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis volutpat nec.</p>
                  <ul class="skill-dots" id="skill-dots">
                  <?php foreach ($professionalSkills as $skill): ?>
                     <li class="skill">
                        <span class="skill-title"><?php echo $skill['skillName']; ?></span>
                        <div class="skill-progress" data-score=" <?php echo $skill['skillScore']; ?>"></div>
                     </li>
                  <?php endforeach; ?>
                  </ul>
               </div>
            </section>
            <!-- =========================================
               WORK
               ========================================== -->
            <section id="experience">
               <div class="col-md-offset-1 col-md-5  offset-lt">
                  <h2 class="title">WORK EXPERIENCE</h2>
                  <hr>
                  <div class="timeline-centered">
                  <?php for($i = 0; $i<$workLen; $i++) {?>
                     <article class="timeline-entry">
                        <div class="timeline-entry-inner">
                           <div class="timeline-icon"></div>
                           <div class="timeline-label">
                           <?php echo $work[$i]['startDate']; ?> - <?php echo $work[$i]['endDate']; ?>
                           </div>
                           <p class="description"><?php echo $work[$i]['period']; ?></p>
                           
                           <h3 class="company"><?php echo $work[$i]['company']; ?></h3>
                           <p class="designation"><?php echo $work[$i]['designation']; ?></p>
                           <p class="description"><?php echo $work[$i]['description']; ?></p>
                        </div>
                     </article>   
                     <?php  } ?>
                  </div>
               </div>
               <div class="col-md-5  offset-rt">
                  <h2 class="title">EDUCATION & DIPLOMAS</h2>
                  <hr>
                  <div class="timeline-centered">
                  <?php foreach ($education as $item): ?>
                     <article class="timeline-entry">
                        <div class="timeline-entry-inner">
                           <div class="timeline-icon"></div>
                           <div class="timeline-label">
                              <?php echo $item['date']; ?>
                           </div>
                           <h3 class="company"><?php echo $item['company']; ?></h3>
                           <p class="designation"><?php echo $item['designation']; ?></p>
                           <p class="description"><?php echo $item['description']; ?></p>
                        </div>
                     </article>
                  <?php endforeach; ?>
                  </div>
               </div>
            </section>
            <!-- =========================================
               PORTFOLIO
               ========================================== -->
            <section id="portfolio">
               <div class="col-md-offset-1 col-md-10">
                  <h2 class="title">PORTFOLIO</h2>
                  <hr>
                  <div class="clearfix"></div>
                  <div class="porfolio-wrapper">
                     <div class="portfolioFilter">
                        <ul>
                           <li>
                              <a href="#" data-filter="*"  class="current">All</a>
                           </li>
                           <li>
                              <a href="#" data-filter=".branding">Branding</a>
                           </li>
                           <li>
                              <a href="#" data-filter=".illustration">Illustration</a>
                           </li>
                           <li>
                              <a href="#" data-filter=".wordpress">Wordpress</a>
                           </li>
                           <li>
                              <a href="#" data-filter=".site-template">Site Template</a>
                           </li>
                        </ul>
                     </div>
                     <ul class="portfolioContainer gallery">
                        <li class="wordpress item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="prettyPhoto[gallery1]" title="Project Name 1" href="http://via.placeholder.com/800x600"><i class="fa fa-image"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                        <li class="branding illustration item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="prettyPhoto[gallery1]" title="Project Name 2" href="http://via.placeholder.com/800x600"><i class="fa fa-image"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                        <li class="illustration item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="prettyPhoto[gallery1]" title="Project Name 3" href="http://via.placeholder.com/400x300"><i class="fa fa-image"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                        <li class="branding illustration item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="prettyPhoto[gallery1]" title="Project Name 4" href="http://via.placeholder.com/800x600"><i class="fa fa-image"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                        <li class="illustration photos item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="prettyPhoto[gallery1]" title="Project Name 5" href="http://via.placeholder.com/800x600"><i class="fa fa-image"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                        <li class="branding photos item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="prettyPhoto[gallery1]" title="Project Name 6" href="https://vimeo.com/244717336"><i class="fa fa-video-camera"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                        <li class="illustration photos item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="prettyPhoto[gallery1]" title="Project Name 7" href="https://www.youtube.com/watch?v=0vrdgDdPApQ"><i class="fa fa-video-camera"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                        <li class="site-template item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="prettyPhoto[gallery1]" title="Project Name 8" href="videos/movie.mp4"><i class="fa fa-video-camera"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </section>
            <!-- =========================================
               REFERENCES
               ========================================== -->
            <section id="references" class="testimonial-outer">
               <div class="col-md-offset-1 col-md-10">
                  <h2 class="title">REFERENCES</h2>
                  <hr>
               </div>
               <div class="clearfix"></div>
               <div class="testimonial-wrapper">
                  <?php $count = count($references);?>
                  <?php $i=1; foreach ($references as $referenc): ?>
                  
                  <div class="<?php if (($i % 2) == 1) {echo "col-md-offset-1 col-md-5 offset-lt";} else{ echo "col-md-5 offset-rt";}?>">
                     
                     <!--testimonial-block-->
                     <div class="testimonial">
                        <div class="testimonialblock">
                           <p><?php echo $referenc['testimonial']; ?></p>
                        </div>
                        <div class="user-wrapper">
                           <img src="<?php echo $referenc['referencImg']; ?>" alt="">
                           <div class="user-detail">
                              <p class="title"><?php echo $referenc['userDetail']; ?></p>
                              <p><?php echo $referenc['designation']; ?>, <?php echo $item['company']; ?></p>
                           </div>
                        </div>
                     </div>
                     <!--testimonial-block ends-->
                  </div>
              
                  <?php $i++; endforeach; ?>
               </div>
            </section>
            <!-- =========================================
               CONNECT ME
               ========================================== -->
            <section id="connect" class="connect padding-bottom-0">
               <div class="col-md-offset-1 col-md-10">
                  <h2 class="title">CONNECT ME</h2>
                  <ul class="social-links social-border">
                     <li class="google-plus"><i class="fa fa-google-plus" aria-hidden="true"></i></li>
                     <li class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></li>
                     <li class="dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></li>
                     <li class="behance"><i class="fa fa-behance" aria-hidden="true"></i></li>
                     <li class="linkdin"><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                     <li class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></li>
                     <li class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></li>
                     <li class="vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></li>
                  </ul>
                  <hr>
               </div>
               <div class="map_outer">
                  <div id="map"></div>
                  <!--contact form-->
                  <div class="contact_form col-md-4 col-sm-7 col-xs-11">
                     <p>Drop me a line</p>
                     <div class="alert alert-success hidden" id="successMessage">
                        <strong>Success!</strong> Your message has been sent to us.
                     </div>
                     <div class="alert alert-danger hidden" id="errorMessage">
                        <strong>Error!</strong> There was an error sending your message.
                     </div>
                     <form method="POST">
                        <div class="col-md-12 ">
                           <?php if(count($errors['name']) > 0): ?>
                              <?php foreach($errors['name'] as $error): ?>
                                 <p><?= $error ?></p>
                              <?php endforeach ?>
                           <?php endif ?>
                           <label>Enter name</label>
                           <input type="text" class="minimal" name="name" id="name" placeholder="Enter name">
                        </div>
                        <div class="col-md-12">
                           <?php if(count($errors['email']) > 0): ?>
                              <?php foreach($errors['email'] as $error): ?>
                                 <p><?= $error ?></p>
                              <?php endforeach ?>
                           <?php endif ?>
                           <label>Enter email</label>
                           <input type="text" class="minimal" name="email" id="email" placeholder="Enter email">
                        </div>
                        <div class="col-md-12 ">
                           <?php if(count($errors['subject']) > 0): ?>
                              <?php foreach($errors['subject'] as $error): ?>
                                 <p><?= $error ?></p>
                              <?php endforeach ?>
                           <?php endif ?>
                           <label>Enter Subject</label>
                           <input type="text" class="minimal" name="subject" id="subject" placeholder="Enter subject">
                           <input type="hidden" name="csrf_token" value="<?= $token ?>">
                        </div>
                        <div class="col-md-12">
                           <?php if(count($errors['message']) > 0): ?>
                              <?php foreach($errors['message'] as $error): ?>
                                 <p><?= $error ?></p>
                              <?php endforeach ?>
                           <?php endif ?>
                           <label>Enter message</label>
                           <textarea class="minimal" name="message" id="message" placeholder="Enter message"></textarea>
                        </div>
                        <div class="col-md-12">
                           <button type="submit" name="submit" class="btn btn-md btn-primary inverse pull-right" value="Submit">Submit</button>
                        </div>
                     </form>
                  </div>
               </div>
            </section>
         </div>
         <!--end of Main container-->
      </div>
      <!-- =========================================
         Footer
         ========================================== -->
      
      <footer>
         © 2017 Flatos.com All right reserved. 
         <a href="#">Theme Elite production</a>
      </footer>
      <!-- =========================
         PRELOADER
         ============================== -->
      <div class="loader-wrapper">
         <div class="preload"></div>
      </div>
      
      <!-- =========================================
         Scripts
         ========================================== -->
      <!-- jQuery -->
      <script src="lib/jquery/jquery-3.2.1.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="lib/bootstrap/js/bootstrap.min.js"></script>
      <!-- JQuery Easing -->
      <script src="lib/jquery/jquery.easing.min.js"></script>
      <!-- Retina LIbrary-->
      <script src="lib/retina/retina.min.js"></script>
      <!--Wow -->
      <script src="lib/wowjs/wow.min.js"></script>
      <!-- Isotope Plugin -->
      <script src="lib/isotope/isotope.pkgd.js"></script>
      <!-- Pretty-Photo Gallery -->
      <script src="lib/pretty-photo/jquery.prettyPhoto.min.js"></script>
      <!-- Jquery Validator -->
      <script src='lib/jquery/jquery.validate.min.js'></script>
      <!-- Google Maps -->
      <script src="lib/googlemaps/infobubble.js"></script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDMQqDC0jmMiZmHAbC_q57Zq3e2obnLMw"></script>
      <script>
         // When the window has finished loading create our google map below
         google.maps.event.addDomListener(window, 'load', init);
         
         function init() {
             // Basic options for a simple Google Map
             // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
             var mapOptions = {
                 // How zoomed in you want the map to start at (always required)
                 zoom: 11,
                 // The latitude and longitude to center the map (always required)
                 center: new google.maps.LatLng(40.702603, -73.788742), // New York
                 // This is where you would paste any style found on Snazzy Maps.
                 styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }]
             };
         
             // Get the HTML DOM element that will contain your map 
             // We are using a div with id="map" seen below in the <body>
             var mapElement = document.getElementById('map');
         
             // Create the Google Map using our element and options defined above
             var map = new google.maps.Map(mapElement, mapOptions);
             var infowindow = new InfoBubble({
                 shadowStyle: 0,
                 padding: 30,
                 backgroundColor: '#fff',
                 borderRadius: 7,
                 arrowSize: 10,
                 borderWidth: 0,
                 maxWidth: 245,
                 borderColor: '#2c2c2c',
                 disableAutoPan: true,
                 hideCloseButton: true,
                 arrowPosition: 30,
                 backgroundClassName: 'transparent',
                 arrowStyle: 0
             });
             var marker = new google.maps.Marker({
                 position: new google.maps.LatLng(40.6700, -73.9400),
                 icon: new google.maps.MarkerImage('img/svg/marker.svg',
                     null, null, null, new google.maps.Size(50, 50)),
                 map: map,
                 title: 'yo'
             });
             google.maps.event.addListener(marker, 'click', (function(marker) {
                 return function() {
                     infowindow.setContent('\u003cp\u003eCollins Street West Victoria 8007 Australia.\u003c/p\u003e\u003cp\u003eCall : +1800 1234 56789\u003c/p\u003e');
                     infowindow.open(map, marker);
                 }
             })(marker));
         }
      </script>
      <!-- Template Custom scripts -->
      <script src="js/scripts.min.js"></script>
   </body>
</html>